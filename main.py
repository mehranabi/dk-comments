from src.extractor import Extractor

print()
print('Welcome!')
print('Don\'t forget to add drivers addresses in `src/constants.py`')
print('Chrome/Edge driver is more stable, and Ajax method is much faster than graphical.')

print()
print('Known Bugs')
print('- App can crash on products with zero comments when using Graphical method!')

print()
driver_name = input('Enter browser name [Chrome, Edge, FireFox]: ')
extraction_mode = input('Enter extraction mode [Ajax, Graphical]: ')

extractor = Extractor(driver_name, extraction_mode)

dk_code = input('Enter DK Product code: ')

comments = extractor.extract(dk_code)

print()
print('{} comments found!'.format(len(comments)))
for comment in comments:
    print('- {}'.format(comment))
