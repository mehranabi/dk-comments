from selenium import webdriver
from src.providers.graphical import GraphicalProvider
from src.providers.ajax import AjaxProvider
from src.constants import *


class Extractor:
    driver = None
    provider = None

    def __init__(self, driver, provider):
        driver = driver.lower()
        if driver != 'chrome' and driver != 'edge' and driver != 'firefox':
            raise Exception('Driver name cannot be {}'.format(driver))

        provider = provider.lower()
        if provider != 'ajax' and provider != 'graphical':
            raise Exception('Provider name cannot be {}'.format(provider))

        address = DRIVERS[driver]

        if driver == 'chrome':
            self.driver = webdriver.Chrome(address)
        elif driver == 'edge':
            self.driver = webdriver.Edge(address)
        elif driver == 'firefox':
            self.driver = webdriver.Firefox(address)
        else:
            raise Exception('Un-supported driver: {}'.format(driver))

        if provider == 'ajax':
            self.provider = AjaxProvider(self.driver)
        elif provider == 'graphical':
            self.provider = GraphicalProvider(self.driver)
        else:
            raise Exception('Un-supported provider: {}'.format(provider))

    def extract(self, product):
        comments = self.provider.extract(product)
        self.driver.close()
        return comments
