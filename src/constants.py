DRIVERS = {
    'chrome': r"C:\webdrivers\chrome_86.exe",
    'edge': r"C:\webdrivers\edge_86.0.622.58.exe",
    'firefox': r"C:\webdrivers\gecko_0.27.0.exe",
}

DK_TEMPLATE = 'https://www.digikala.com/product/dkp-{}'
DK_AJAX_TEMPLATE = 'https://www.digikala.com/ajax/product/comments/list/{}'

PAGE_LOAD = 15
COMMENT_LOAD = 5

AJAX_PAGE_LOAD = 5
AJAX_COMMENT_LOAD = 3
