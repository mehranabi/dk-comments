class Common:
    @staticmethod
    def next(driver, current_page, ajax=False):
        try:
            if ajax:
                pagination = driver.find_element_by_id('comment-pagination')
            else:
                container = driver.find_element_by_id('product-comment-list')
                pagination = container.find_element_by_id('comment-pagination')

            i_list = pagination.find_element_by_tag_name('ul')
            items = i_list.find_elements_by_tag_name('a')

            for item in items:
                page = item.get_attribute('data-page')
                if str(page).isnumeric() and int(page) > current_page:
                    Common.click(driver, item)
                    return int(page)
        except:
            return -1

        return -1

    @staticmethod
    def click(driver, element):
        driver.execute_script('arguments[0].click();', element)

    @staticmethod
    def extract(driver, ajax=False):
        comments = []

        if ajax:
            c_list = driver.find_element_by_class_name('c-comments__list')
        else:
            container = driver.find_element_by_id('product-comment-list')
            c_list = container.find_element_by_tag_name('ul')

        items = c_list.find_elements_by_xpath('*')

        for item in items:
            comment = Common.text(item)
            comments.append(comment)

        return comments

    @staticmethod
    def text(element):
        section = element.find_element_by_tag_name('section')
        article = section.find_element_by_class_name('article')
        return article.find_element_by_tag_name('p').text
