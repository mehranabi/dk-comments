from time import sleep
from src.providers.common import Common
from src.constants import *


class GraphicalProvider:
    driver = None
    comments = []
    page = 1

    def __init__(self, driver):
        self.driver = driver

    def extract(self, product):
        self.driver.get(DK_TEMPLATE.format(product))
        sleep(PAGE_LOAD)

        self.click_on_comments_tab()
        sleep(COMMENT_LOAD)

        self.comments.extend(Common.extract(self.driver))

        self.page = Common.next(self.driver, self.page)
        while self.page != -1:
            sleep(COMMENT_LOAD)

            new_comments = Common.extract(self.driver)
            self.comments.extend(new_comments)

            self.page = Common.next(self.driver, self.page)

        return self.comments

    def click_on_comments_tab(self):
        tabs = self.driver.find_element_by_id('tabs').find_element_by_tag_name('ul')
        comments_tab = tabs.find_element_by_xpath('//li[@data-method="comments"]')
        Common.click(self.driver, comments_tab)
