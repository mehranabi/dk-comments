from time import sleep
from src.providers.common import Common
from src.constants import *


class AjaxProvider:
    driver = None
    comments = []
    page = 1

    def __init__(self, driver):
        self.driver = driver

    def extract(self, product):
        self.driver.get(DK_AJAX_TEMPLATE.format(product))
        sleep(AJAX_PAGE_LOAD)

        self.comments.extend(Common.extract(self.driver, True))

        self.page = Common.next(self.driver, self.page, True)
        while self.page != -1:
            sleep(AJAX_COMMENT_LOAD)

            new_comments = Common.extract(self.driver, True)
            self.comments.extend(new_comments)

            self.page = Common.next(self.driver, self.page, True)

        return self.comments
