### DigiKala Comments Extractor

This project is using Selenium in Python, to extract comments of a product in [DigiKala](https://digikala.com) website.

### Run
- Clone the project: `git clone https://gitlab.com/mehranabi/dk-comments.git`
- Make sure you are using `Python 3.x`
- Install `selenium`: `pip install selenium`
- Download WebDriver for Chrome, Edge or FireFox:
    - **Chrome** webdriver: https://chromedriver.chromium.org/downloads
    - **Edge** webdriver: https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
    - **FireFox** webdriver: https://github.com/mozilla/geckodriver
- Set your webdriver addrese in `constants.py` file
- Run app: `python main.py`

### Usage

When you first start the application, it asks you to choose your desired WebDriver, you must have that browser installed and have the WebDriver address set in `constants.py` file *(refer to Run part)*.

After that, you need to give DigiKala product id:

DigiKala product url is something like this: `https://www.digikala.com/product/dkp-2062220`. The `dkp-2062220` part is what we need, because it has the product id (`2062220`).

For any product on DigiKala, copy product id of the product and paste in in application.

### Change

By default, application goes through all pages of comments section and extracts all comments for a product, and store them in `comments` array. And after finished, it prints the count of comments. You can do anything with comments by changing `main.py` file, or copy `Extractor.py` class to your own project.

### Developer
- S. Mehran Abghari
    - mehran.ab80@gmail.com
    - GitLab: [@mehranabi](https://gitlab.com/mehranabi)
    - GitHub: [@mehranabi](https://github.com/mehranabi)
